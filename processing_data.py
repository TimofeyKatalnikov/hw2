import os

from pyspark import SparkContext

FILE_NAME = 'data.txt'


def delete_first_rows_from_file(file_name, n_rows):
    """
    Сгруппировать события по метке времени

    :param file_name: Имя файла с данными
    :type file_name: str
    :param n_rows: Количество строк для удаления
    :type n_rows: int
    """
    n = n_rows
    nfirstlines = []

    with open(file_name) as f, open("tmp.txt", "w") as out:
        for x in range(n):
            nfirstlines.append(next(f))
        for line in f:
            out.write(line)

    os.remove(file_name)
    os.rename("tmp.txt", file_name)


def group_events(file_name=FILE_NAME, log_level='INFO', test=False):
    """
    Сгруппировать события по метке времени

    :param file_name: Имя файла с данными
    :type file_name: str
    :param log_level: Уровень логирования
    :type log_level: str (choices: ALL, DEBUG, WARN, ...)
    :param test: Запускается ли тестом
    :type test: bool
    :return: Количество событий, сгруппированных по метке времени
    :rtype: list(tuple)
    """
    sc = SparkContext(appName="TweetDeleteEvents")
    sc.setLogLevel(log_level)

    with open(file_name) as f:
        lines = f.readlines()

    n_rows = len(lines)
    if n_rows == 0:
        return []

    # Найти последнюю границу законченного ts и обрезать, удалив все границы
    lines.reverse()
    end_index = lines.index('-\n')
    lines = lines[end_index:]
    lines = list(filter(lambda a: a != '-\n', lines))

    # Удалить из файла считанные строки
    if not test:
        delete_first_rows_from_file(file_name, n_rows - end_index)

    data = [(int(x.replace('\n', '')), 1) for x in lines]
    rdd = sc.parallelize(data)
    grouped_data = sorted(rdd.groupByKey().mapValues(len).collect())

    sc.stop()
    return grouped_data


if __name__ == '__main__':
    events_by_minutes = group_events()
    #print(events_by_minutes)
