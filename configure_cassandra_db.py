from cassandra.cluster import Cluster
from cassandra.policies import DCAwareRoundRobinPolicy
from cassandra.protocol import ConfigurationException

KEYSPACE = 'twitter'


def drop_keyspace(session, keyspace):
    """
    Удалить keyspace

    :param session: Объект текущей сессии
    :param keyspace: Название keyspace
    :type keyspace: str
    """
    session.execute("DROP KEYSPACE " + keyspace)


def configure_keyspace(session, keyspace):
    """
    Создать keyspace и таблицу для хранения данных

    :param session: Объект текущей сессии
    :param keyspace: Название keyspace
    :type keyspace: str
    """
    session.execute(
        """
        CREATE KEYSPACE %s
        WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' }
        """ % keyspace
    )
    session.set_keyspace(keyspace)

    session.execute(
        """
        CREATE TABLE delete_events (
        ts int,
        delete_events_count int,
        PRIMARY KEY (ts)
        )
        """
    )


if __name__ == '__main__':
    cluster = Cluster(['localhost'], load_balancing_policy=DCAwareRoundRobinPolicy())
    session = cluster.connect()

    try:
        drop_keyspace(session, KEYSPACE)
    except ConfigurationException:
        pass

    configure_keyspace(session, KEYSPACE)
    print('Cassandra initialized...\n')
