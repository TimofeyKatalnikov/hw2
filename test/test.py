import unittest

from cassandra.cluster import Cluster
from cassandra.policies import DCAwareRoundRobinPolicy
from time import sleep

from cassandra import ConsistencyLevel
from cassandra.cluster import Cluster
from cassandra.policies import DCAwareRoundRobinPolicy
from cassandra.query import SimpleStatement
from configure_cassandra_db import configure_keyspace, drop_keyspace
from processing_data import group_events


class Test(unittest.TestCase):
    DATA_FILE_NAME = 'test_data.txt'
    TEST_KEYSPACE = 'test32'

    def test_spark_grouping(self):
        grouped_data = group_events(file_name=self.DATA_FILE_NAME, test=True)
        self.assertEqual([(111, 4), (222, 5)], grouped_data)

    def test_writing_to_cassandra(self):
        cluster = Cluster(['localhost'], load_balancing_policy=DCAwareRoundRobinPolicy())
        session = cluster.connect()
        configure_keyspace(session, self.TEST_KEYSPACE)

        query = SimpleStatement("""
                        INSERT INTO delete_events (ts, delete_events_count)
                        VALUES (%(ts)s, %(delete_events_count)s)
                        """, consistency_level=ConsistencyLevel.ONE)
        session.execute(query, dict(ts=123, delete_events_count=222))

        future = session.execute_async("SELECT * FROM delete_events")
        rows = future.result()

        self.assertEqual(rows[0][0], 123)
        self.assertEqual(rows[0][1], 222)

        drop_keyspace(session, self.TEST_KEYSPACE)


if __name__ == '__main__':
    unittest.main()
