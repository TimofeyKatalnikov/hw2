from time import sleep

from cassandra import ConsistencyLevel
from cassandra.cluster import Cluster
from cassandra.policies import DCAwareRoundRobinPolicy
from cassandra.query import SimpleStatement

from processing_data import group_events

KEYSPACE = 'twitter'
PERIODICITY = 62  # sec


def save_to_cassandra(session, keyspace):
    """
    Сохранить данные в Cassandra

    :param session: Объект текущей сессии
    :param keyspace: Название keyspace
    :type keyspace: str
    """
    grouped_events = group_events()

    query = SimpleStatement("""
                INSERT INTO delete_events (ts, delete_events_count)
                VALUES (%(ts)s, %(delete_events_count)s)
                """, consistency_level=ConsistencyLevel.ONE)

    session.set_keyspace(keyspace)
    for event in grouped_events:
        session.execute(query, dict(ts=event[0], delete_events_count=event[1]))


def print_cassandra_data(session):
    """
    Вывести данные из Cassandra

    :param session: Объект текущей сессии
    """
    future = session.execute_async("SELECT * FROM delete_events")
    rows = future.result()

    for row in rows:
        print(row)


if __name__ == '__main__':
    cluster = Cluster(['localhost'], load_balancing_policy=DCAwareRoundRobinPolicy())
    session = cluster.connect()

    print('Start writing data to Cassandra...\n')
    while True:
        save_to_cassandra(session, KEYSPACE)
        print_cassandra_data(session)

        sleep(PERIODICITY)
