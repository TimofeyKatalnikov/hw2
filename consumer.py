import json
from datetime import datetime

from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener

CONSUMER_KEY = 'PbHuTRuSqSHTUzAJGJuqZAZoz'
CONSUMER_SECRET = 'BjpUlQLT7lVuXR7kI0PJCYS77b6hFDuQKYLLex7ETfKRYDiLpX'
ACCESS_TOKEN = '319641898-eSXscJnl6aTyExpEZUCVi6RyQ8JzRPIQ4R9qFDA5'
ACCESS_TOKEN_SECRET = '1AYcrO4wodtFxVZKWv5UB5v7eKgZq21wJ8xaTRYkvhCKU'


class Consumer(StreamListener):
    last_ts = None

    def on_data(self, raw_data):
        """
        Функция-обработчик, которая вызывается при каждом событии
        (обрабатываются только события удаления,
        метка времени округляется до минуты перед записью)

        :param raw_data: Данные о событии
        :type raw_data: str
        :return: Успех обработки
        :rtype: bool
        """
        data = json.loads(raw_data)

        if 'delete' in data:
            timestamp_sec = int(data['delete']['timestamp_ms']) / 1000
            timestamp_min = int(datetime.timestamp(
                datetime.fromtimestamp(timestamp_sec).replace(second=0)
            ))

            # Иногда от API приходит неверный timestamp огромной длины
            if len(str(timestamp_min)) > 10:
                print(f'wrong ts={timestamp_min} from API')
                # Просто пропустить
                return True

            with open('data.txt', 'a+') as f:
                if self.last_ts != timestamp_min:
                    f.write(f'-\n')
                f.write(f'{timestamp_min}\n')

            self.last_ts = timestamp_min

        return True


if __name__ == '__main__':
    consumer = Consumer()
    auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    stream = Stream(auth, consumer)
    stream.sample()
